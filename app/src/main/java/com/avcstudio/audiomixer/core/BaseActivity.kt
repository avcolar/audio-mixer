package com.avcstudio.audiomixer.core

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.avcstudio.audiomixer.R

open class BaseActivity(private val layoutResourceId: Int) : AppCompatActivity() {

    private val mainFragmentFactory = MainFragmentFactory()

    override fun onCreate(savedInstanceState: Bundle?) {
        supportFragmentManager.fragmentFactory = mainFragmentFactory
        super.onCreate(savedInstanceState)
        setContentView(layoutResourceId)
    }

    public fun renderFragment(fragment: Fragment) {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.container, fragment)
            .commit()
    }
}
