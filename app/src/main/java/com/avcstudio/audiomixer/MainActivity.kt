package com.avcstudio.audiomixer

import android.os.Bundle
import com.avcstudio.audiomixer.core.BaseActivity

class MainActivity : BaseActivity(R.layout.activity_main) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }
}
